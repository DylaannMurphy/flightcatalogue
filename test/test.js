// Import the dependencies for testing
var chai = require('chai');
var chaiHttp = require('chai-http');
var app = require('../server');
// Configure chai
chai.use(chaiHttp);
chai.should();
var assert = require('assert')

describe("API Requirements", () => {
    const express = require('express');
    const bodyParser = require('body-parser');
    const sql = require("mssql");
    const connection = require("../connection/connection")();

    describe("Express", () => {
        it("Is installed", () => {
            try {
                require.resolve("Express");
            } catch (e) {
                console.error("Express is not found");
                assert();
            }
        })
    })

    describe("JSON Body Parser", () => {
        it("Is installed", () => {
            try {
                require.resolve('body-parser');
            } catch (e) {
                console.error("JSON Body Parser is not found");
                assert();
            }
        })
    })

    describe("SQL", () => {
        it("Is installed", () => {
            try {
                require.resolve('mssql');
            } catch (e) {
                console.error("mssql is not found");
                assert();
            }
        })
    })

    describe("SQL Connection", () => {
        it("Connected to SQL Database", () => {
            assert.notEqual(connection, null);
        })
    })


})

describe("Connection API", () => {
    describe("Controller", () => {
        it("Controller exists", () => {
            try {
                require.resolve('../connection/connection');
            } catch (e) {
                console.error("Connection not found");
                assert();
            }
        })
    })
})

describe("Flights API", () => {
    describe("Controller", () => {
        it("Controller exists", () => {
            try {
                require.resolve('../controllers/flightcontroller');
            } catch (e) {
                console.error("Flights API controller is not found");
                assert();
            }
        })
    })

    //Too much data to get for this to work yet
    describe("GET /", () => {
        it("should get all Flight record", (done) => {
            chai.request(app)
                .get('/api/flights')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        })
    });

    describe("GET /", () => {
        it("should get a single Flight record", (done) => {
            chai.request(app)
                .get('/api/flights/flightid/V001')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array')
                    done();
                });
        })
    });
})

describe("Airports API", () => {
    describe("Controller", () => {
        it("Controller exists", () => {
            try {
                require.resolve('../controllers/airportscontroller');
            } catch (e) {
                console.error("Airports API controller is not found");
                assert();
            }
        })
    })

    describe("GET /", () => {
        it("should get all Airport record", (done) => {
            chai.request(app)
                .get('/api/airports')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        })
    });

    describe("GET /", () => {
        var data = 'HZK';
        it("should get a single Airport record", (done) => {
            chai.request(app)
                .get('/api/airports/searchIATA/' + data)
                .send(data)
                .end((err, res) => {
                    assert(res.body.Airport_Name != 'HUSAVIK');
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        })
    });

    describe("GET /", () => {
        var data = 'IRELAND'
        it("should get Airports in a country", (done) => {
            chai.request(app)
                .get('/api/airports/country/' + data)
                .send(data)
                .end((err, res) => {
                    assert(res.body[0].Country == data);
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        })
    });

    describe("GET /", () => {
        var data = 'DUBLIN'
        it("should get Airports in a city", (done) => {
            chai.request(app)
                .get('/api/airports/city/' + data)
                .end((err, res) => {
                    assert(res.body[0].City_Town == data);
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        })
    });

    describe("GET /", () => {
        var data = 'LON'
        it("should return airport with similar data", (done) => {
            chai.request(app)
                .get('/api/airports/searchareaflights/' + data)
                .end((err, res) => {
                    if(res.body[0].City_Town != data || res.body[0].Country || res.body[0].Airport_Name)
                    { 
                        assert(-1, "Results not found");
                    }
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        })
    });
})

describe("Airplanes API", () => {
    describe("Controller", () => {
        it("Controller exists", () => {
            try {
                require.resolve('../controllers/airplanecontroller');
            } catch (e) {
                console.error("Airplanes API controller is not found");
                assert();
            }
        })
    })

    describe("GET /", () => {
        it("should get all Airplane record", (done) => {
            chai.request(app)
                .get('/api/airplanes')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        })
    });

    describe("GET /", () => {
        it("should get a single Airplane record", (done) => {
            chai.request(app)
                .get('/api/airplanes/Plane1')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        })
    });

    describe("POST /", () => {
        let data = {
            "PlaneID": "1",
            "Capacity": 2,
            "Status": "dummy",
            "Flights": "dummy"
        };
        it("should add a single Airplane record", (done) => {
            chai.request(app)
                .post('/api/airplanes/addplane')
                .send(data)
                .end((err, res) => {
                    res.should.have.status(202);
                    if (err) return done(err);
                    done();
                });
        })
    });

    describe("DELETE /", () => {
        let data = {
            "PlaneID": "1"
        };
        it("should delete a single Airplane record", (done) => {
            chai.request(app)
                .delete('/api/airplanes/:id')
                .send(data)
                .end((err, res) => {
                    res.should.have.status(200);
                    if (err) return done(err);
                    done();
                });
        })
    });
})