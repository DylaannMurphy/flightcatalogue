const express = require("express");
const router = express.Router();
const sql = require("mssql");
const connection = require("../connection/connection")();
const tableName = "Flights";

const routes = function () {
	router.route('/')
		// Get all flights
		.get(function (req, res) {
			connection.connect().then(function () {
				let sqlQuery = `SELECT * FROM ${tableName}`;
				let req = new sql.Request(connection);
				req.query(sqlQuery).then(function (recordset) {
					res.json(recordset.recordset);
					connection.close();
					apiMonitor("Flights: Get All Flight Data", "Success");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Flights: " + err, "Failed");
					});
			});
		})
		// Fill db with flights NEVER USE THIS UNLESS POPULATING DATABASE
		.post(function (req, res) {
			//generateFlights(req, res);
		});

	// Get Flight with its id in route parameter
	router.route('/flightid/:id')
		.get(function (req, res) {
			let _id = req.params.id;
			connection.connect().then(function () {
				let sqlQuery = `SELECT * FROM ${tableName} WHERE Flight_ID = @id`;
				let req = new sql.Request(connection);
				req.input("id", sql.NVarChar(10), _id);
				req.query(sqlQuery).then(function (recordset) {
					res.status(200).json(recordset.recordset);
					connection.close();
					apiMonitor("Flights: Get Flight By ID", "Success");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Flights: " + err, "Failed");
					});
			})
				.catch(function (err) {
					connection.close();
					res.status(400).send("Connection Error while getting data " + err);
					apiMonitor("Flights: " + err, "Failed");
				});
		});

	// Get Flights Miles
	router.route('/flightmiles/:id')
		.get(function (req, res) {
			let _id = req.params.id;
			connection.connect().then(function () {
				let sqlQuery = `SELECT Miles FROM ${tableName} WHERE Flight_ID = @id`;
				let req = new sql.Request(connection);
				req.input("id", sql.NVarChar(10), _id);
				req.query(sqlQuery).then(function (recordset) {
					res.status(200).json(recordset.recordset);
					connection.close();
					apiMonitor("Flights: Get Flight Miles By ID", "Success");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Flights: " + err, "Failed");
					});
			})
				.catch(function (err) {
					connection.close();
					res.status(400).send("Connection Error while getting data " + err);
					apiMonitor("Flights: " + err, "Failed");
				});
		});

	// Put  - Update a flights status
	router.route('/changeflightstatus/:id/:status')
		.put(function (req, res) {
			connection.connect().then(function () {
				let transaction = new sql.Transaction(connection);
				transaction.begin().then(function () {
					let request = new sql.Request(transaction);
					let _id = req.params.id;
					request.input("id", sql.VarChar(10), _id);
					request.input("Status", sql.VarChar(50), req.body.status);
					request.query("UPDATE Flights SET status = @status WHERE id = @id").then(function () { // For stored procedures
						transaction.commit().then(function (recordSet) {
							connection.close();
							res.status(200).send(req.body);
						}).catch(function (err) {
							connection.close();
							res.status(400).send("Transaction Commit Error while updating data");
						});
					}).catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while updating data");
					});
				}).catch(function (err) {
					connection.close();
					res.status(400).send("Transaction Begin Error while updating data");
				});
			}).catch(function (err) {
				connection.close();
				res.status(400).send("Connection Error while updating data");
			});
		});


	/*Swathi - added - start */
	router.route('/viewname/:view_name')
		.get(function (req, res) {
			console.log("Inside view Route");
			let view_name = (req.params.view_name);
			console.log("viewname:" + view_name);
			connection.connect().then(function () {
				let sqlQuery = `SELECT * FROM ${view_name}`;
				//console.log("Query for View: "+sqlQuery);
				let req = new sql.Request(connection);
				req.query(sqlQuery).then(function (recordset) {
					res.json(recordset.recordset);
					connection.close();
					res.status(200).send(req.body);
				}).catch(function (err) {
					connection.close();
					res.status(400).send("Transaction Commit Error while updating data");
				});
			}).catch(function (err) {
				connection.close();
				res.status(400).send("Database Error while updating data");
			});
		});

	/*Swathi - added - start */
	router.route('/viewname/:view_name')
		.get(function (req, res) {
			console.log("Inside view Route");
			let view_name = (req.params.view_name);
			console.log("viewname:" + view_name);
			connection.connect().then(function () {
				let sqlQuery = `SELECT * FROM ${view_name}`;
				console.log("Query for View: " + sqlQuery);
				let req = new sql.Request(connection);
				req.query(sqlQuery).then(function (recordset) {
					res.json(recordset.recordset);
					connection.close();
					apiMonitor("Flights: View Query", "Success");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Flights: " + err, "Failed");
					});
			})//connection
				.catch(function (err) {
					connection.close();
					res.status(400).send("Connection Error while getting data " + err);
					apiMonitor("Flights: " + err, "Failed");
				});
		})

	router.route('/Graphs_Date_Status/:StartD/:EndD/:Stat')
		.get(function (req, res) {
			console.log("Inside Graphs_Date with status");
			let paramA = "";
			let paramB = "";
			let paramC = "";
			paramA = ((req.params.StartD).substring(0, 11));
			paramB = ((req.params.EndD).substring(0, 11));
			paramC = (req.params.Stat);
			paramC = (paramC.substring(0, (paramC.length)));
			connection.connect().then(function () {
				let sqlQuery = `select count(*) as Number_of_Flights,CAST(depart_date as DATE) as Week_dates from ${tableName}
							where CAST(depart_date as DATE) between '${paramA}' and '${paramB}' and status='${paramC}'
							group by CAST(depart_date as DATE)`;
				//console.log("sqlQuery::"+sqlQuery);
				let req = new sql.Request(connection);
				req.input("Depart_Date", sql.Date, paramA);
				req.input("Depart_Date", sql.Date, paramB);
				req.input("status", sql.NVarChar(20), paramC);
				req.query(sqlQuery).then(function (recordset) {
					res.json(recordset.recordset);
					connection.close();
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
					});
			})//connection
				.catch(function (err) {
					connection.close();
					res.status(400).send("Database Error while getting data " + err);
					apiMonitor("Flights: " + err, "Failed");
				});
		})

	router.route('/Graphs_Date/:StartD/:EndD')
		.get(function (req, res) {
			console.log("Inside Graphs_Date");
			let paramA = "";
			let paramB = "";
			paramA = ((req.params.StartD).substring(0, 11));
			paramB = ((req.params.EndD).substring(0, 11));
			connection.connect().then(function () {
				let sqlQuery = `SELECT * FROM ${tableName} WHERE CAST(depart_date as DATE) between '${paramA}' and '${paramB}'`;
				console.log(sqlQuery);
				let req = new sql.Request(connection);
				req.input("Depart_Date", sql.Date, paramA);
				req.input("Depart_Date", sql.Date, paramB);
				req.query(sqlQuery).then(function (recordset) {
					res.json(recordset.recordset);
					connection.close();//Swathi
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
					});
			})//connection
				.catch(function (err) {
					connection.close();
					res.status(400).send("Connection Error while getting data " + err);
				});
		})

	//Post - Updating total no of passengers based on Flight ID.
	router.route('/AddPassengersCount/:FlightID/:AddSeats')
		.post(function (req, res) {
			console.log("Add Seats to Specified Flight ID");
			let fid = req.body.FlightID;
			let addseats = req.body.AddSeats;
			connection.connect().then(function () {
				let transaction = new sql.Transaction(connection);
				transaction.begin().then(function () {
					let request = new sql.Request(transaction);
					request.input("Flight_ID", sql.VarChar(50), fid);
					request.input("Seats_filled", sql.Int, addseats);
					request.query(`UPDATE Flights set Seats_filled= (Seats_filled+${addseats}) where Flight_ID='${fid}'`).then(function () { // For updating Flights
						transaction.commit().then(function (recordSet) {
							connection.close();
							res.status(200).send(req.body);
						}).catch(function (err) {
							connection.close();
							res.status(400).send("Transaction Commit Error while Updating Passengers Count");
						});
					}).catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while Updating data " + err);
					});
				}).catch(function (err) {
					connection.close();
					res.status(400).send("Transaction Begin Error while Updating data");
				});
			});
		})
	//Post - Reducing no of passengers based on Flight ID.
	router.route('/ReducePassengersCount/:FlightID/:RemoveSeats')
		.post(function (req, res) {
			console.log("Remove Seats from Specified Flight ID");
			let fid = req.body.FlightID;
			let removeseats = req.body.RemoveSeats;
			connection.connect().then(function () {
				let transaction = new sql.Transaction(connection);
				transaction.begin().then(function () {
					let request = new sql.Request(transaction);
					request.input("Flight_ID", sql.VarChar(50), fid);
					request.input("Seats_filled", sql.Int, removeseats);
					request.query(`UPDATE Flights set Seats_filled= (Seats_filled-${removeseats}) where Flight_ID='${fid}'`).then(function () { // For updating Flights
						transaction.commit().then(function (recordSet) {
							connection.close();
							res.status(200).send(req.body);
						}).catch(function (err) {
							connection.close();
							res.status(400).send("Transaction Commit Error while Updating Passengers Count");
						});
					}).catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while Updating data " + err);
					});
				}).catch(function (err) {
					connection.close();
					res.status(400).send("Transaction Begin Error while Updating data");
				});
			});
		})

	//Confirmation-Get data on basis of Flight ID and departure Date
	router.route('/Confirmation/:FlightId/:StartD')
		.get(function (req, res) {
			console.log("Inside Confirmation details");
			let paramA = "";
			let paramB = "";
			paramA = ((req.params.StartD).substring(0, 11));
			paramB = (req.params.FlightId);
			connection.connect().then(function () {
				let sqlQuery = `SELECT * FROM ${tableName} WHERE CAST(depart_date as DATE) ='${paramA}' and Flight_ID= '${paramB}'`;
				console.log("Fetch Confirmation details: " + sqlQuery);
				let req = new sql.Request(connection);
				req.input("Depart_Date", sql.Date, paramA);
				req.input("Flight_ID", sql.VarChar(100), paramB);
				req.query(sqlQuery).then(function (recordset) {
					res.json(recordset.recordset);
					connection.close();
					apiMonitor("Flights: Get Flight Confirmation", "Success");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Flights: " + err, "Failed");
					});
			})//connection
				.catch(function (err) {
					connection.close();
					res.status(400).send("Connection Error while getting data " + err);
					apiMonitor("Flights: " + err, "Failed");
				});
		})
	/*Swathi - added - Ends */

	router.route('/suggestion')
		.get(function (req, res) {
			connection.connect().then(function () {
				let request = new sql.Request(connection);
				request.execute("spmostvisited").then(function (result) {
					res.json(result.recordset);
					console.log("in suggestion- API- result: " + result["Destination"]);
					connection.close();
					apiMonitor("Flights: Stored Procedure:spmostvisited", "Success");
				})
					.catch(function (err) {
						console.log("Error::" + err);
						connection.close();
						res.status(400).send(err);
						apiMonitor("Flights: " + err, "Failed");
					});
			})
		});

	// Most visted countries from ireland
	router.route('/suggestions')
		.get(function (req, res) {
			connection.connect().then(function () {
				let request = new sql.Request(connection);
				request.execute("spMVPireland").then(function (result) {
					res.json(result.recordset);
					console.log("in suggestion- API- result: " + result["Destination_Country"]);
					connection.close();
				})
					.catch(function (err) {
						console.log("Error::" + err);
						connection.close();
						res.status(400).send(err);
					});
			})
			//Cheap flight from Dublin
			router.route('/suggestionss')
				.get(function (req, res) {
					connection.connect().then(function () {
						let request = new sql.Request(connection);
						request.execute("sp_cheapflight").then(function (result) {
							res.json(result.recordset);
							console.log("in suggestion- API- result: " + result["Destination_Country"]);
							connection.close();
						})
							.catch(function (err) {
								console.log("Error::" + err);
								connection.close();
								res.status(400).send(err);
							});
					})
				});
		})

	// Return status of a specific flight
	router.route('/Status/:id')
		.get(function (req, res) {
			let _id = req.params.id;
			connection.connect().then(function () {
				let sqlQuery = `SELECT * FROM ${tableName} WHERE Flight_ID = @id`;
				let req = new sql.Request(connection);
				req.input("id", sql.NChar, _id);
				req.query(sqlQuery).then(function (recordset) {
					res.json(recordset.recordset);
					connection.close();
					apiMonitor("Flights: Search For Flight Status", "Success");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Flights: " + err, "Failed");
					});
			})
				.catch(function (err) {
					connection.close();
					res.status(400).send("Connection Error while getting data " + err);
					apiMonitor("Flights: " + err, "Failed");
				});
		});

	//status
	router.route('/getFlights/:leavingdate/:fromairport/:toairport/:passengers')
		.get(function (req, res) {
			connection.connect().then(function () {
				var leavingdate = req.params.leavingdate;
				var fromairport = req.params.fromairport;
				var toairport = req.params.toairport;
				var passengers = req.params.passengers;
				var request = new sql.Request(connection);
				request.input("leavingdate", sql.Date, leavingdate);
				request.input("fromairport", sql.VarChar(99), fromairport);
				request.input("toairport", sql.VarChar(99), toairport);
				request.input("passengers", sql.Int, passengers);
				// Execute stored procedure to add to db
				request.execute("spgetflights").then(function (recordset) {
					connection.close();
					res.json(recordset.recordset);
					apiMonitor("Flights: Stored Procedure:spgetflights", "Success");
				})
			})
				.catch(function (err) {
					connection.close();
					res.status(400).send("Connection Error while getting data " + err);
					apiMonitor("Flights: " + err, "Failed");
				});
		})

	return router;
};

// ‘haversine’ formula
function getDistance(airport1, airport2) {

	var R = 6371; // Estimated radius of the world in meters
	var rad1 = (airport1.Latitude_Degrees).toRad(); // Convert to radians
	var rad2 = (airport2.Latitude_Degrees).toRad();
	var distLat = (airport2.Latitude_Degrees - airport1.Latitude_Degrees).toRad(); // Difference in Longitude & Latitude
	var distLong = (airport2.Longitude_Degrees - airport1.Longitude_Degrees).toRad();

	// Mixing degrees and radians
	var a = Math.sin(distLat / 2) * Math.sin(distLat / 2) +
		Math.cos(rad1) * Math.cos(rad2) *
		Math.sin(distLong / 2) * Math.sin(distLong / 2);

	// Atan2 - angle in the Euclidean plane
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

	// Distance "As the crow flies" on ground, increase r by flight height to get more acccurate results
	var distance = R * c;

	return distance;
}

// Convert degrees to radians
if (typeof (Number.prototype.toRadians) === "undefined") {
	Number.prototype.toRad = function () {
		return this * Math.PI / 180;
	}
}

// Cost is calculated by getting the relative diatance cost of 600 km.
function calcCost(distance) {
	var costPrice = (distance / 600) * 35;

	return costPrice
}

// Random Number generator
function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Random date generator
function randomDate(start, end) {
	return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

// Function to sleep
function sleep(ms) {
	return new Promise(resolve => {
		setTimeout(resolve, ms)
	})
}

// Fill db with flights NEVER USE THIS UNLESS POPULATING TABLE
function generateFlights(req, res) {
	connection.connect().then(function () {
		var airportsList;
		var airplanesList;
		var request = new sql.Request(connection);
		// Get data from airplanes & flights
		request.query("SELECT * FROM Airplanes").then(function (recordset) {
			airplanesList = recordset.recordset;
			var request2 = new sql.Request(connection);
			request2.query("SELECT * FROM Airports").then(function (recordset2) {
				airportsList = recordset2.recordset;

				// Initialise Date
				var today = new Date();
				var flightsInDay = 0;
				var year = 2020;
				var month = 01;
				var day = 1;

				// Loop for number of flights to fill the database
				for (var flightNum = 201; flightNum < 400; flightNum++) {
					// Setup random flights, destinations and aiplanes (can be refined at a later stage)
					var request = new sql.Request(connection);
					flightsInDay++;
					var fromDestNum = getRandomInt(0, airportsList.length - 1);
					var fromToNum = getRandomInt(0, airportsList.length - 1);
					var airplaneNum = getRandomInt(0, airplanesList.length - 1);
					while (fromDestNum == fromToNum) {
						fromToNum = getRandomInt(0, airportsList.length - 1);
					}
					// Loop for flights in a day & Alter dates
					if (flightsInDay > 5) {
						day++;
						flightsInDay = 0;
						if (day == 28 && month == 2) {
							month++;
							day = 1;
							flightsInDay = 0;
						}
						else if (day == 30 && (month == 9 || month == 4 || month == 6 || month == 10)) {
							month++
							day = 1;
							flightsInDay = 0;
						}
						else if (day == 31) {
							month++;
							day = 1;
							flightsInDay = 0;
						}
					}
					else {
						today = randomDate(new Date(year, month, day, 0, 0, 0), new Date(year, month, day, 22, 59, 0));
					}

					// Create random flights using data
					var fromDest = airportsList[fromDestNum].Airport_Name;
					var toDest = airportsList[toDestNum].Airport_Name;
					var currentAirplane = airplanesList[airplaneNum].PlaneID;
					var totalSeats = airplanesList[airplaneNum].Capacity;
					var filledSeats = 0;
					var status = 'On Time';
					var flightDist = getDistance(airportsList[fromDestNum], airportsList[toDestNum]);
					var departDate = new Date(today);
					var flightTime = (flightDist / 545);
					if (today.getHours() + flightTime >= 24) {
						flightTime -= 24;
						today.setDate(today.getDate() + 1);
					}
					today.setHours(today.getHours() + flightTime);
					var arriveDate = today;
					var flightCost = calcCost(flightDist);
					// Creating row to add to db
					request.input("Flight_ID_1", sql.NVarChar(50), "V" + (flightNum + 1));
					request.input("FROM_1", sql.NVarChar(200), fromDest);
					request.input("TO_1", sql.NVarChar(200), toDest);
					request.input("Depart_date_1", sql.DateTime, departDate);
					request.input("Arrive_Date_1", sql.DateTime, arriveDate);
					request.input("Total_Seats_1", sql.Int, totalSeats);
					request.input("Seats_Filled_1", sql.Int, filledSeats);
					request.input("AirplaneID_1", sql.NVarChar(20), currentAirplane);
					request.input("Status_1", sql.NVarChar(20), status);
					request.input("Miles_1", sql.Float, flightDist);
					request.input("Price_1", sql.Float, flightCost);

					console.log("about to execute" + flightNum);

					// Execute stored procedure to add to db
					request.execute("dbo.spAddFlights").then(function () {
						console.log("uploading... ");
						apiMonitor("Flights: Adding Flights", "Success");
					})
						.catch(function (err) {
							connection.close();
							res.status(400).send("Executing stored Procedures: " + err);
							apiMonitor("Flights: " + err, "Failed");
						});
				}
			});
		});
	});
}

// Function to monitor api useage and record to db
function apiMonitor(apiCalled, result) {
	/*connection.connect().then(function () {
		var request = new sql.Request(connection);
		request.input("spcalled", sql.VarChar(300), apiCalled);
		request.input("time", sql.DateTime, new Date());
		request.input("result", sql.VarChar(20), result);

		request.execute("apimonitor").then(function (recordset) {
			connection.close();
		})
	})*/
}
module.exports = routes;
