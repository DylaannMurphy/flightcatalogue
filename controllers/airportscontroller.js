const express = require("express");
const router = express.Router();
const sql = require("mssql");
const connection = require("../connection/connection")();
const tableName = "Airports";

const routes = function () {
	// Route to return all airports in database
	router.route('/')
		.get(function (req, res) {
			connection.connect().then(function () {
				let sqlQuery = `SELECT * FROM ${tableName}`;
				let req = new sql.Request(connection);
				req.query(sqlQuery).then(function (recordset) {
					res.json(recordset.recordset);
					connection.close();
					apiMonitor("Airport: All Search", "Success");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Airport: " + err, "Failed");
					});
			})
				.catch(function (err) {
					connection.close();
					res.status(400).send("Connection Error while getting data " + err);
					apiMonitor("Airport: " + err, "Failed");
				});
		});

	// Route to return specific airport given the code
	router.route('/:id')
		.get(function (req, res) {
			let _id = req.params.id;
			connection.connect().then(function () {
				let sqlQuery = `SELECT * FROM ${tableName} WHERE ICAO_Code = @id`;
				let req = new sql.Request(connection);
				req.input("id", sql.NChar, _id);
				req.query(sqlQuery).then(function (recordset) {
					res.json(recordset.recordset);
					connection.close();
					apiMonitor("Airport: IACO Code Search", "Sucess");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Airport: " + err, "Failed");
					});
			})
				.catch(function (err) {
					connection.close();
					res.status(400).send("Connection Error while getting data " + err);
					apiMonitor("Airport: " + err, "Failed");
				});
		});

	// Return list of airports in a country
	router.route('/country/:id')
		.get(function (req, res) {
			let _id = req.params.id;
			connection.connect().then(function () {
				let sqlQuery = `SELECT * FROM ${tableName} WHERE Country = @id`;
				let req = new sql.Request(connection);
				req.input("id", sql.NChar, _id);
				req.query(sqlQuery).then(function (recordset) {
					res.status(200).json(recordset.recordset);
					connection.close();
					apiMonitor("Airport: Country Search", "Sucess");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Airport: " + err, "Failed");
					});
			})
				.catch(function (err) {
					connection.close();
					res.status(400).send("Connection Error while getting data " + err);
					apiMonitor("Airport: " + err, "Failed");
				});
		});

	// Return list of airports in a city
	router.route('/city/:id')
		.get(function (req, res) {
			let _id = req.params.id;
			connection.connect().then(function () {
				let sqlQuery = `SELECT * FROM ${tableName} WHERE City_Town = @id`;
				let req = new sql.Request(connection);
				req.input("id", sql.NChar, _id);
				req.query(sqlQuery).then(function (recordset) {
					res.status(200).json(recordset.recordset);
					connection.close();
					apiMonitor("Airport: Airports in City Search", "Sucess");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Airport: " + err, "Failed");
					});
			})
				.catch(function (err) {
					connection.close();
					res.status(400).send("Connection Error while getting data " + err);
					apiMonitor("Airport: " + err, "Failed");
				});
		});

	router.route('/searchareaflights/:letters')
		// Return airports, countries and cities given first few letters.
		.get(function (req, res) {
			connection.connect().then(function () {
				var request = new sql.Request(connection);
				var letters = req.params.letters;
				request.input("letters", sql.VarChar(99), letters);
				// Execute stored procedure to add to db
				request.execute("destsearch").then(function (recordset) {
					connection.close();
					res.status(200).json(recordset.recordset);
					apiMonitor("Airport: Area Search With Letters", "Sucess");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Airport: " + err, "Failed");
					});
			});
		})

	router.route('/searchIATA/:iata')
		// Get airport data related to iata code
		.get(function (req, res) {
			connection.connect().then(function () {
				var request = new sql.Request(connection);
				var iata = req.params.iata;
				request.input("iata", sql.VarChar(10), iata);
				// Execute stored procedure to add to db
				request.execute("spiatasearch").then(function (recordset) {
					connection.close();
					res.json(recordset.recordset);
					apiMonitor("Airport: IATA Search", "Success");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Airport: " + err, "Failed");
					});
			});
		})

	return router;
};
module.exports = routes;

// Function to monitor api useage and record to db
function apiMonitor(apiCalled, result) {
	/*connection.connect().then(function () {
		var request = new sql.Request(connection);
		request.input("spcalled", sql.VarChar(300), apiCalled);
		request.input("time", sql.DateTime, new Date());
		request.input("result", sql.VarChar(20), result);

		request.execute("apimonitor").then(function (recordset) {
			connection.close();
		})
	})*/
}
