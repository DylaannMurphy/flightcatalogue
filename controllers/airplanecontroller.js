const express = require("express");
const router = express.Router();
const sql = require("mssql");
const connection = require("../connection/connection")();
const tableName = "Airplanes";

const routes = function () {
	// Route to return all Plane Records from database
	router.route('/')
		.get(function (req, res) {
			connection.connect().then(function () {
				let sqlQuery = `SELECT * FROM ${tableName}`;
				let req = new sql.Request(connection);
				req.query(sqlQuery).then(function (recordset) {
					res.json(recordset.recordset);
					connection.close();
					apiMonitor("Airplane: All Airplane Data Request", "Success");
				}).catch(function (err) {
					connection.close();
					res.status(400).send("Database Error while getting data " + err);
					apiMonitor("Airplane: " + err, "Failed");
				});
			}).catch(function (err) {
				connection.close();
				res.status(400).send("Connection Error while getting data " + err);
				apiMonitor("Airplane: " + err, "Failed");
			});
		});

	// Get - Route to return a specific plane given id.
	// Delete - Delete a specific plane from database.
	router.route('/:id')
		.get(function (req, res) {
			let _id = req.params.id;
			connection.connect().then(function () {
				let sqlQuery = `SELECT * FROM ${tableName} WHERE PlaneID = @id`;
				let req = new sql.Request(connection);
				req.input("id", sql.NChar, _id);
				req.query(sqlQuery).then(function (recordset) {
					res.status(200).json(recordset.recordset);
					connection.close();
					apiMonitor("Airplane: Plane ID Request", "Success");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Airplane: " + err, "Failed");
					});
			})
				.catch(function (err) {
					connection.close();
					res.status(400).send("Connection Error while getting data " + err);
					apiMonitor("Airplane: " + err, "Failed");
				});
		})
		.delete(function (req, res) {
			connection.connect().then(function () {
				let _id = req.params.id;
				let transaction = new sql.Transaction(connection);
				transaction.begin().then(function () {
					let request = new sql.Request(transaction);
					request.input("id", sql.VarChar(10), _id);
					request.query("DELETE FROM Airplanes WHERE PlaneID = @id").then(function () { // For stored procedures
						transaction.commit().then(function (recordSet) {
							connection.close();
							res.status(200).send(req.params.id);
							apiMonitor("Airplane: Delete Plane Request", "Success");
						}).catch(function (err) {
							connection.close();
							res.status(400).send("Transaction Commit Error while deleting data");
							apiMonitor("Airplane: " + err, "Failed");
						});
					}).catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while deleting data");
						apiMonitor("Airplane: " + err, "Failed");
					});
				}).catch(function (err) {
					connection.close();
					res.status(400).send("Transaction Begin Error while deleting data");
					apiMonitor("Airplane: " + err, "Failed");
				});
			}).catch(function (err) {
				connection.close();
				res.status(400).send("Connection Error while deleting data");
				apiMonitor("Airplane: " + err, "Failed");
			});
		})

	// Route to add a new plane to database
	router.route('/addplane')
		.post(function (req, res) {
			connection.connect().then(function () {
				let transaction = new sql.Transaction(connection);
				transaction.begin().then(function () {
					let request = new sql.Request(transaction);
					request.input("PlaneID", sql.NVarChar(255), req.body.PlaneID);
					request.input("Capacity", sql.Int(), req.body.Capacity);
					request.input("Status", sql.NVarChar(255), req.body.Status);
					request.input("Flights", sql.NVarChar(255), req.body.Flights);

					request.query("INSERT INTO Airplanes(PlaneID, Capacity, Status, Flights) VALUES(@PlaneID, @Capacity, @Status, @Flights)").then(function () {
						transaction.commit().then(function (recordSet) {
							connection.close();
							res.status(202).send(req.body);
							apiMonitor("Airplane: Add A Plane Request", "Success");
						}).catch(function (err) {
							connection.close();
							res.status(400).send("Transaction Commit Error while inserting data");
							apiMonitor("Airplane: " + err, "Failed");
						});
					}).catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while inserting data");
						apiMonitor("Airplane: " + err, "Failed");
					});
				}).catch(function (err) {
					connection.close();
					res.status(400).send("Transaction Begin Error while inserting data");
					apiMonitor("Airplane: " + err, "Failed");
				});
			}).catch(function (err) {
				connection.close();
				res.status(400).send("Connection Error while inserting data");
				apiMonitor("Airplane: " + err, "Failed");
			});
		})

	//for status with plane ID
	router.route('/Status/:id')
		.get(function (req, res) {
			let _id = req.params.id;
			connection.connect().then(function () {
				let sqlQuery = `SELECT PlaneID,Status FROM ${tableName} WHERE PlaneID = @id`;
				let req = new sql.Request(connection);
				req.input("id", sql.NChar, _id);
				req.query(sqlQuery).then(function (recordset) {
					res.json(recordset.recordset);
					connection.close();
					apiMonitor("Airplane: Plane Status", "Sucess");
				})
					.catch(function (err) {
						connection.close();
						res.status(400).send("Database Error while getting data " + err);
						apiMonitor("Airplane: " + err, "Failed");
					});
			})
				.catch(function (err) {
					connection.close();
					res.status(400).send("Connection Error while getting data " + err);
					apiMonitor("Airplane: " + err, "Failed");
				});
		});


	return router;
};
module.exports = routes;

// Function used to send data to an api monitoring table in sql
function apiMonitor(apiCalled, result) {
	/*connection.connect().then(function () {
		var request = new sql.Request(connection);
		request.input("spcalled", sql.VarChar(300), apiCalled);
		request.input("time", sql.DateTime, new Date());
		request.input("result", sql.VarChar(20), result);

		request.execute("apimonitor").then(function (recordset) {
			connection.close();
		})
	})*/
}