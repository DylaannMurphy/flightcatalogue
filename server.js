const express = require('express');
const app = express();
const port = 3000;
const bodyParser = require('body-parser');

// create application/x-www-form-urlencoded parser
app.use(bodyParser.urlencoded({ extended: true }));

// create application/json parser
app.use(bodyParser.json());
app.use((request, response, next) => {
  response.header("Access-Control-Allow-Origin", "*");
  response.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, PATCH");
  response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Create controllers for api
const flightsController = require('./controllers/flightcontroller')();
app.use('/api/flights', flightsController);
const airportsController = require('./controllers/airportscontroller')();
app.use('/api/airports', airportsController);
const airplaneController = require('./controllers/airplanecontroller')();
app.use('/api/airplanes', airplaneController);

// Start the server
app.listen(port, function () {
  let datetime = new Date();
  console.log("Server runnning on Port:- " + port + "\nStarted at : " + datetime);
});

module.exports = app;
